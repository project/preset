<?php
/**
 * Implementation of hook_features_export_options().
 */
function preset_features_export_options() {
  $options = array();
  foreach ( preset_get_presets() as $preset ) {
    $options[$preset['module'] . '_' . $preset['name']] = $preset['module'] . ': ' . $preset['title'];
  }
  return $options;
}

/**
 * Implementation of hook_features_export().
 */
function preset_features_export($data, &$export, $module_name = '') {
  foreach ($data as $preset) {
    $export['features'][$module_name][$preset] = $preset;
  }
}

/**
 * Implementation of hook_features_export_render().
 */
function preset_features_export_render($module_name, $data) {
  $items = array();
  foreach ($data as $key) {
    $items[$key] = preset_get( $module_name, $key );
  }
  $code = "  \$items = ". features_var_export($items, '  ') .";\n";
  $code .= '  return $items;';
  return array( $module_name . '_default_presets' => $code);
}

/**
 * Implementation of hook_features_revert().
 */
function preset_features_revert($module) {
  if ($default_presets = features_get_default($module, $module)) {
    foreach (array_keys($default_presets) as $default_preset) {
       preset_delete($module, $default_preset);
    }
  }
}
