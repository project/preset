<?php

/**
 * The hook that tells the preset module that your module should be added to the Preset Management.
 *
 * @return <array>
 *   title - The title that you would like to show in the preset manager.
 *
 *   description - The description of your preset when it is shown in the preset manager.
 *
 *   permissions - The permission that you would like to add for the management of your preset.
 *
 *   default_settings - The default settings for your preset.  This can also be passed in as a string,
 *      which is then interpreted as a function callback that has the following signature...
 *
 *      function mymodule_default_settings( $preset ) {
 *        return array(
 *          'param1' => 1,
 *          'param2' => 2
 *        );
 *      }
 *
 *   file - The external file that will hold the form hooks (seen below) for your preset.
 */
function hook_preset_info() {
  return array(
    'title' => t('My Preset'),
    'description' => t('The description of my preset for my module.'),
    'permissions' => array(
      'name' => 'administer my preset',
      'title' => t('Administer My Preset'),
      'description' => t('The description of my preset permission.')
    ),
    'default_settings' => array(
      'param1' => 0,
      'param2' => 1,
      'param3' => 3,
      'param4' => 4
    ),
    'file' => drupal_get_path('module', 'mymodule') .'/includes/mymodule.preset.inc'
  );
}

/**
 * This is used to set some required settings while the preset is being created.
 *
 * @return <array> - The form elements that you would like to add for the settings.
 *
 *                   Special Note:  It is required to structure all your settings using
 *                   $form['preset']['settings'] as the parent form element.  Otherwise,
 *                   they will not get serialized within the "settings" column of the database.
 */
function hook_preset_create_form() {
  $form = array();

  $form['preset']['settings']['param1'] = array(
    '#type' => 'textfield',
    '#title' => t('My Parameter'),
    '#description' => t('This is a parameter that is required on the preset creation.')
  );

  return $form;
}

/**
 * This is used to add all the other settings that you need to declare in your preset.
 * The preset object is passed to this form, so you can use that data to populate the default values
 * for your form elements by using $preset['settings']['param2'].
 *
 * @param <array> - The preset object.
 * @return <array> - The additional form settings that you would like to add to your preset.
 */
function hook_preset_form( $preset ) {
  $form = array();

  $form['preset']['settings']['param2'] = array(
    '#type' => 'textfield',
    '#title' => t('Other parameter'),
    '#description' => t('This is another paramter needed for our preset.'),
    '#default_value' => $preset['settings']['param2']
  );

  return $form;
}